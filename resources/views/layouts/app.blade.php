<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        VISUALISASI DATA KEMISKINAN DI INDONESIA
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            {{-- <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li> --}}
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('bower_components/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('bower_components/chart.js/dist/Chart.bundle.js') }}"></script>
    <script src="{{ asset('bower_components/chart.js/dist/utils.js') }}"></script>
    <script type="text/javascript">
var barChartData = {
            labels: ["ACEH", "SUMATERA UTARA", "RIAU", "JAMBI", "SUMATERA SELATAN", "BENGKULU", "LAMPUNG", "DKI JAKARTA", "JAWA TENGAH", "DI YOGYAKARTA", "BALI", "NUSA TENGGARA BARAT", "NUSA TENGGARA TIMUR", "KALIMANTAN UTARA", "SULAWESI UTARA", "MALUKU", "PAPUA"],
            datasets: [{
                label: 'Semester 1',
                backgroundColor: window.chartColors.red,
                data: [
                    296406+312801,
                    263932+296832,
                    293851+312352,
                    262791+284973,
                    250850+269320,
                    298505+324851,
                    253869+274437,
                    319595+329644,
                    216823+231673,
                    237473+252284,
                    221931+234393,
                    235036+250371,
                    238070+256245,
                    341484+367944,
                    230475+246007,
                    306768+318165,
                    302807+321910
                ]
            }, {
                label: 'Semester 2',
                backgroundColor: window.chartColors.blue,
                data: [
                    305428+323548,
                    279015+308037,
                    306835+321762,
                    273957+292122,
                    260885+275036,
                    324764+335717,
                    267028+276216,
                    327678+334938,
                    226501+236403,
                    246776+255304,
                    228017+238822,
                    241112+250737,
                    245160+258985,
                    353538+382698,
                    238209+246173,
                    310278+324381,
                    305579+331243
                ]
            }]

        };
        // window.onload = function() {
        //     var ctx = document.getElementById("canvas").getContext("2d");
        //     window.myBar = new Chart(ctx, {
        //         type: 'bar',
        //         data: barChartData,
        //         options: {
        //             title:{
        //                 display:true,
        //                 text:"Garis Kemiskinan Makanan"
        //             },
        //             tooltips: {
        //                 mode: 'index',
        //                 intersect: false
        //             },
        //             responsive: true,
        //             scales: {
        //                 xAxes: [{
        //                     stacked: true,
        //                 }],
        //                 yAxes: [{
        //                     stacked: true
        //                 }]
        //             }
        //         }
        //     });
        // };


var barChartData2 = {
            labels: ["ACEH", "SUMATERA UTARA", "RIAU", "JAMBI", "SUMATERA SELATAN", "BENGKULU", "LAMPUNG", "DKI JAKARTA", "JAWA TENGAH", "DI YOGYAKARTA", "BALI", "NUSA TENGGARA BARAT", "NUSA TENGGARA TIMUR", "KALIMANTAN UTARA", "SULAWESI UTARA", "MALUKU", "PAPUA"],
            datasets: [{
                label: 'Semester 1',
                backgroundColor: window.chartColors.red,
                data: [
                    93744+98155,
                    84021+91324,
                    105361+113648,
                    81144+86903,
                    76055+82664,
                    81523+91576,
                    84127+90485,
                    167793+180715,
                    81028+85675,
                    98413+101800,
                    99903+104574,
                    79202+83624,
                    59793+66702,
                    134137+145670,
                    64890+71471,
                    92864+96137,
                    99224+105265
                ]
            }, {
                label: 'Semester 2',
                backgroundColor: window.chartColors.blue,
                data: [
                    96344+101217,
                    87123+93795,
                    110329+115497,
                    84470+87525,
                    80073+86661,
                    86076+101468,
                    89744+92376,
                    175361+185752,
                    82814+86345,
                    100945+104865,
                    103011+107576,
                    81577+85836,
                    62064+68018,
                    139548+147868,
                    68895+72811,
                    95001+100275,
                    100806+108778
                ]
            }]

        };
        window.onload = function() {
            var ctx2 = document.getElementById("canvas2").getContext("2d");
            window.myBar2 = new Chart(ctx2, {
                type: 'bar',
                data: barChartData2,
                options: {
                    title:{
                        display:true,
                        text:"Garis Kemiskinan Non Makanan 2015-1016"
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            });

            var ctx = document.getElementById("canvas").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    title:{
                        display:true,
                        text:"Garis Kemiskinan Makanan 2015-1016"
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            });
        };
    </script>
</body>
</html>
