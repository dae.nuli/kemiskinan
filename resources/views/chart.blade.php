@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data 1</div>

                <div class="panel-body">
                    <canvas id="canvas"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data 2</div>

                <div class="panel-body">
                    <canvas id="canvas2"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
